﻿using UnityEngine;
using System.Collections;
using Assets.ToonGames.BehaviorDesigner.Runtime.FSM;
using Assets.ToonGames.BehaviorDesigner.Runtime.FSM.Conditions;
using UnityEngine.UI;


public static class FsmParameters
{
    public const string Parameter1 = "Parameter1";
    public const string Parameter2 = "Parameter2";
}

public static class States
{
    public const string State1 = "State1";
    public const string State2 = "State2";
    public const string State3 = "State3";
    public const string State4 = "State4";
}

public class State1 : State
{
    public State1()
        : base(States.State1)
    {
    }
}

public class State2 : State
{
    public State2()
        : base(States.State2)
    {
    }
}

public class State3 : State
{
    public State3()
        : base(States.State3)
    {
    }
}

public class State4 : State
{
    public State4()
        : base(States.State4)
    {
    }
}

public class FSMTest : MonoBehaviour
{
    public Text TransactionText;

    private  StateMachine _stateMachine;

	// Use this for initialization
	void Start () {

        _stateMachine = new StateMachine();
        _stateMachine.OnChangedState += (from, to) =>
        {
            ShowTransaction(from.Name + "->" + to.Name);
        };

        // Инициализируем переменные для перехода
        _stateMachine.Parameters.AddInt(FsmParameters.Parameter1);
        _stateMachine.Parameters.AddBool(FsmParameters.Parameter2);


        // Добавляем состояния
        _stateMachine.AddState(new State1());
        _stateMachine.AddState(new State2());
        _stateMachine.AddState(new State3());
        _stateMachine.AddState(new State4());

       // Устанавливаем состояние по умолчанию
        _stateMachine.SetAsDefualt(States.State1);
        ShowTransaction(States.State1);

	    _stateMachine.GetState(States.State1).Transitions.AutoCheckConditions = true;
	    _stateMachine.GetState(States.State2).Transitions.AutoCheckConditions = true;
	    _stateMachine.GetState(States.State3).Transitions.AutoCheckConditions = true;
	    _stateMachine.GetState(States.State4).Transitions.AutoCheckConditions = true;

        // Устанавливаем переходы между состояниями при условии выполнения условий
        _stateMachine.GetState(States.State1).Transitions.AddToState(States.State2).Conditions.Add(_stateMachine.Parameters.GetBool(FsmParameters.Parameter2), true);
        _stateMachine.GetState(States.State2).Transitions.AddToState(States.State1).Conditions.Add(_stateMachine.Parameters.GetBool(FsmParameters.Parameter2), false);

        _stateMachine.GetState(States.State1).Transitions.AddToState(States.State3).Conditions.Add(_stateMachine.Parameters.GetInt(FsmParameters.Parameter1), 10, CompareType.Equals);
        _stateMachine.GetState(States.State3).Transitions.AddToState(States.State1).Conditions.Add(_stateMachine.Parameters.GetInt(FsmParameters.Parameter1), 0, CompareType.Equals);

        _stateMachine.GetState(States.State2).Transitions.AddToState(States.State3).Conditions.Add(_stateMachine.Parameters.GetInt(FsmParameters.Parameter1), 10, CompareType.Greater);
        
        _stateMachine.GetState(States.State3).Transitions.AddToState(States.State4).Conditions.Add(_stateMachine.Parameters.GetInt(FsmParameters.Parameter1), 100, CompareType.Greater);
        _stateMachine.GetState(States.State3).Transitions.AddToState(States.State4).Conditions.Add(_stateMachine.Parameters.GetInt(FsmParameters.Parameter1), 150, CompareType.Less);
	}

    private void ShowTransaction(string value)
    {
        if (TransactionText)
            TransactionText.text = value;
    }

    // Update is called once per frame
	void Update () {
	
	}

    public void SetParameter2(bool value)
    {
        _stateMachine.Parameters.SetBool(FsmParameters.Parameter2, value);
    }

    public void SetParameter1(string value)
    {
        var intValue = 0;
        if (int.TryParse(value, out  intValue))
        {
            _stateMachine.Parameters.SetInt(FsmParameters.Parameter1, intValue);        
        }
    }
}
