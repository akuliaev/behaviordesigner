﻿using System;
using Assets.ToonGames.BehaviorDesigner.Runtime.FSM;

namespace Assets.ToonGames.BehaviorDesigner.Runtime
{
    public sealed class BehaviorMachineIntermediary
    {
        private Action<string, string> _onChangedState;
        public event Action<string, string> OnChangedState
        {
            add { _onChangedState += value; }
            remove { _onChangedState -= value; }
        }

        public string BehaviorStateName { get; private set; }

        public BehaviorMachineIntermediary(StateMachine fms)
        {
            fms.OnChangedState += OnChangedStateHandler;
        }

        public void OnChangedStateHandler(State from, State to)
        {
            BehaviorStateName = to.Name;
            if (_onChangedState != null)
                _onChangedState(from.Name, to.Name);
        }
    }
}
