﻿using Assets.ToonGames.BehaviorDesigner.Runtime.FSM.Conditions;

namespace Assets.ToonGames.BehaviorDesigner.Runtime.FSM
{
    public enum StateTransitionStatus
    {
        Closed,
        Open
    }

    public class StateTransition
    {
        private StateTransitionStatus _status;
        public StateTransitionStatus Status
        {
            get { return _status; }
            private set {
                if (_status != value)
                {
                    _status = value;

                    if (_onChangedStatus != null) _onChangedStatus(this);
                }
            }
        }

        private System.Action<StateTransition> _onChangedStatus;
        public event System.Action<StateTransition> OnChangedStatus {
            add { _onChangedStatus += value; }
            remove { _onChangedStatus -= value; }
        }

        public bool IsActivated { get; private set; }
        public readonly string From;
        public readonly string To;

        public readonly StateTransitionConditions Conditions;

        public StateTransition(string from, string to)
        {
            From = from;
            To = to;

            Conditions = new StateTransitionConditions();
        }

        public void Activate()
        {
            if (IsActivated) return;

            IsActivated = true;
            //Debug.Log("------ Activate StateTransition " + From + " -> " + To);

            Conditions.OnConfirmed += OnConfirmedHandler;

        }

        public void Deactivate()
        {
            if (!IsActivated) return;
            IsActivated = false;
            //Debug.Log("------ Deactivate StateTransition " + From + " -> " + To);

            Conditions.OnConfirmed -= OnConfirmedHandler;


        }

        private void OnConfirmedHandler()
        {
            //Debug.Log("StateTransition OnConfirmedHandler");
            Open();
        }

        public void Open()
        {
            //Debug.Log("StateTransition TryToOpen Status:" + Status);

            if (Status != StateTransitionStatus.Closed) return;

            //Debug.Log("StateTransition TryToOpen Status:" + Status);

            Status = StateTransitionStatus.Open;
        }

        public void Closed()
        {
            Status = StateTransitionStatus.Closed;
        }
    }
}