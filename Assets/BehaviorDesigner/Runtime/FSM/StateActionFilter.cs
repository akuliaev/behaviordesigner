﻿using System.Collections.Generic;

namespace Assets.ToonGames.BehaviorDesigner.Runtime.FSM
{
    public sealed class StateActionFilter
    {
        public readonly string State;

        private readonly List<string> _actions; 

        public StateActionFilter(string state, params string[] actions)
        {
            State = state;

            _actions = new List<string>();
            _actions.AddRange(actions);
        }

        public void AddAction(string action)
        {
            if(HasAction(action)) return;
            _actions.Add(action);
        }

        public bool HasAction(string action)
        {
            return _actions.Contains(action);
        }
    }
}
