﻿using System;
using Assets.ToonGames.BehaviorDesigner.Runtime.FSM.Parameters;

namespace Assets.ToonGames.BehaviorDesigner.Runtime.FSM.Conditions
{
    [Flags]
    public enum CompareType
    {
        Greater,
        Less,
        Equals,
        NotEqual

    }

    public interface IStateTransitionCondition
    {
        int Index { get; }
        event System.Action<IStateTransitionCondition> OnConfirmed;
        bool IsConfirmed { get; }

        void Check();
    }

    public class StateTransitionCondition<T> : IStateTransitionCondition
    {
        private Action<IStateTransitionCondition> _onConfirmed;
        public event Action<IStateTransitionCondition> OnConfirmed
        {
            add { _onConfirmed += value; }
            remove { _onConfirmed -= value; }
        }

        public int Index { get; private set; }

        private readonly StateMachineParameter<T> _parameter;

        private readonly StateMachineParameters _parameters;

        private readonly CompareType _compareType;

        private readonly T _value;

        public StateTransitionCondition(int index, StateMachineParameter<T> parameter, T value, CompareType compareType)
        {
            Index = index;
            _parameter = parameter;
            _parameter.OnChanged += OnParameterChangedHandler;
            _value = value;
            _compareType = compareType;
        }

        private void OnParameterChangedHandler(StateMachineParameter<T> parameter)
        {            
            Check();
            
            if (IsConfirmed)
            {
                //Debug.Log("-------------IsConfirmed " + parameter.Value + " IsConfirmed : " + IsConfirmed + " _value: " + _value);
                DispatchConfirmedEvent();
            }
        }

        public void Check()
        {
            //Debug.Log("Check " + _parameter.Value + " IsConfirmed : " + IsConfirmed + " _value: " + _value);
            IsConfirmed = _parameter.Compare(_value, _compareType);
        }

        public bool IsConfirmed { get; private set; }


        protected void DispatchConfirmedEvent()
        {
            if (_onConfirmed != null) _onConfirmed(this);
        }
    }
}
