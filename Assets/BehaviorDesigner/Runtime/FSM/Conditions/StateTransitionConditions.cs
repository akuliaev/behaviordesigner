﻿using System.Collections.Generic;
using Assets.ToonGames.BehaviorDesigner.Runtime.FSM.Parameters;

namespace Assets.ToonGames.BehaviorDesigner.Runtime.FSM.Conditions
{
    public class StateTransitionConditions {

        private System.Action _onConfirmed;
        public event System.Action OnConfirmed
        {
            add { _onConfirmed += value; }
            remove { _onConfirmed -= value; }
        }

        private readonly Dictionary<IStateMachineParameter, List<IStateTransitionCondition>> _conditions;

        public StateTransitionConditions()
        {
            _conditions = new Dictionary<IStateMachineParameter, List<IStateTransitionCondition>>();
        }

        private void AddCondition<T>(StateMachineParameter<T> parameter, T value, CompareType compareType)
        {
            if (parameter == null) return;

            if (!_conditions.ContainsKey(parameter))
                _conditions.Add(parameter, new List<IStateTransitionCondition>());

            var condition = new StateTransitionCondition<T>(_conditions[parameter].Count, parameter, value, compareType);
            condition.OnConfirmed += OnConfirmedHandler;
            _conditions[parameter].Add(condition);
        }

        public void Add(SmpInt parameter, int value, CompareType compareType)
        {
            AddCondition(parameter, value, compareType);
        }

        public void Add(SmpFloat parameter, float value, CompareType compareType)
        {
            AddCondition(parameter, value, compareType);
        }

        public void Add(SmpBool parameter, bool value)
        {
            AddCondition(parameter, value, CompareType.Equals);
        }

        public void Add(SmpString parameter, string value, bool isEquals)
        {
            AddCondition(parameter, value, isEquals ? CompareType.Equals : CompareType.NotEqual);
        }

        public void RemoveAt(IStateMachineParameter parameter, int index)
        {
            if (!Has(parameter, index)) return;

            var condition = _conditions[parameter][index];
            condition.OnConfirmed -= OnConfirmedHandler;

            _conditions[parameter].Remove(condition);
        }

        private bool Has(IStateMachineParameter parameter, int index)
        {
            return parameter != null && _conditions.ContainsKey(parameter) && _conditions[parameter].Find(c=>c.Index == index) != null;
        }

        private void OnConfirmedHandler(IStateTransitionCondition condition)
        {
            Check();
        }

        public void Check()
        {
            //Debug.Log("-------Condition Check");
            foreach (var pair in _conditions)
            {
                var list = pair.Value;
                for (int i = 0; i < list.Count; i++)
                {
                    var c = list[i];
                    c.Check();
                    if (!c.IsConfirmed) return;
                }
            }

            //Debug.Log("-------Condition _onConfirmed");

            if (_onConfirmed != null) _onConfirmed();
        }
    }
}
