﻿using System;
using Assets.ToonGames.BehaviorDesigner.Runtime.FSM.Parameters;

namespace Assets.ToonGames.BehaviorDesigner.Runtime.FSM
{
    public class StateMachine
    {

        private Action<State,State> _onChangedState;
        public event Action<State,State> OnChangedState
        {
            add { _onChangedState += value; }
            remove { _onChangedState -= value; }
        }

        public readonly StateMachineParameters Parameters;

        public State State { get; private set; }

        public bool IsEnabled { get; private set; }

        private readonly StatesPool _statesPool;
        private string _defaultState;


        public StateMachine()
        {
            Parameters = new StateMachineParameters();
            _statesPool = new StatesPool();      
        }

        public virtual void SetAsDefualt(State state)
        {
            if (state == null) return;

            SetAsDefualt(state.Name);
        }

        public virtual void SetAsDefualt(string stateName)
        {
            if (GetState(stateName) == null) return;

            _defaultState = stateName;
            SetState(_defaultState);
        }

        public virtual void AddState(State state)
        {
            if (state == null) return;
        
            _statesPool.AddState(state);
        }

        public void RemoveState(State state)
        {
            _statesPool.RemoveState(state);
        }

        public State GetState(string stateName)
        {
            return _statesPool.GetState(stateName);
        }

        public void SetState(string stateName)
        {
            var prevState = State != null ? State.Name : _defaultState;
            var state = _statesPool.GetState(stateName);

            if (state == null) return;
            State = state;
            State.SetStateMachine(this);
            State.Activate();

            if (prevState != State.Name)
            {
               // Debug.Log("--------- {State: " + State.Name + "} ------------");

                if (_onChangedState != null)
                    _onChangedState(GetState(prevState), State);
            }
        }

        public void Reset()
        {
            SetState(_defaultState);
        }

        public void Enable()
        {
            IsEnabled = true;
        }

        public void Disable()
        {
            IsEnabled = false;
        }
    }
}