﻿using System;
using System.Collections.Generic;

namespace Assets.ToonGames.BehaviorDesigner.Runtime.FSM
{
    public class StateActionFiltersManager
    {
        private readonly StateMachine _stateMachine;
        private readonly List<StateActionFilter> _filters;

        public StateActionFiltersManager(StateMachine stateMachine)
        {
            _stateMachine = stateMachine;
            _filters = new List<StateActionFilter>();
        }

        public StateActionFilter AddFilter(string state, params string[] actions)
        {
            var filter = GetFilter(state);
            if (filter != null) return filter;

            filter = new StateActionFilter(state, actions);
            _filters.Add(filter);

            return filter;
        }

        public bool HasFilter(string state)
        {
            return _filters.Find(c => c.State == state) != null;
        }

        public StateActionFilter GetFilter(string state)
        {
            return _filters.Find(c => c.State == state);
        }

        public bool CanPerformAction(string actionName)
        {
            if (String.IsNullOrEmpty(actionName)) return false;

            foreach (var transition in _stateMachine.State.Transitions)
            {
                var filter = GetFilter(transition.To);

                if (filter != null && filter.HasAction(actionName))
                {
                    return true;
                }
            }

            return false;
        }
    }
}