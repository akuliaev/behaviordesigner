﻿namespace Assets.ToonGames.BehaviorDesigner.Runtime.FSM
{
    public interface IBechaviorLogic
    {
        void Update();
    }

    public abstract class State
    {
        public readonly string Name;

        public bool IsActivated{ get; private set; }

        public StateMachine StateMachine { get; private set; }

        public readonly StateTransitionManager Transitions;

        public State(string nameState)
        {
            Name = nameState;
            Transitions = new StateTransitionManager(nameState, OnTransitionOpenHandler);

        }

        private void OnTransitionOpenHandler(StateTransition transition)
        {
            if (!IsActivated) return;

            if (transition.To != Name)
            {
                transition.Closed();
                Deactivate();
                StateMachine.SetState(transition.To);
                
            }
        }

        public virtual void Activate()
        {
            if (IsActivated) return;
            IsActivated = true;

            //Debug.Log("State: " + Name + " Activated");
            //Transitions.OnTransitionOpen += OnTransitionOpenHandler;

            Transitions.Activate();
            if (Transitions.AutoCheckConditions)
            {
                foreach (var transition in Transitions)
                {
                    transition.Conditions.Check();
                }
            }
        }
        public virtual void Deactivate()
        {
            if (!IsActivated) return;            
            IsActivated = false;
            //Debug.Log("State: " + Name + " Deactivate");

            Transitions.Deactivate();

            //Transitions.OnTransitionOpen -= OnTransitionOpenHandler;
        }


        public void SetStateMachine(StateMachine stateMachine)
        {
            StateMachine = stateMachine;
        }
    }
}
