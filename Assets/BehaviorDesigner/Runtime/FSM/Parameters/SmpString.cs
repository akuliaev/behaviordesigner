﻿using System;
using Assets.ToonGames.BehaviorDesigner.Runtime.FSM.Conditions;

namespace Assets.ToonGames.BehaviorDesigner.Runtime.FSM.Parameters
{
    public sealed class SmpString : StateMachineParameter<string>
    {
        public override bool Compare(string value, CompareType compareType)
        {
            if (Value == null) return false;
            switch (compareType)
            {
                case CompareType.Equals: return Value.Equals(value);
                case CompareType.NotEqual: return !Value.Equals(value);
                default: return false;
            }
        }

        public override void Reset()
        {
            Value = String.Empty;
        }
    }
}
