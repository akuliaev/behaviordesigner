﻿using System;
using Assets.ToonGames.BehaviorDesigner.Runtime.FSM.Conditions;

namespace Assets.ToonGames.BehaviorDesigner.Runtime.FSM.Parameters
{
    public interface IStateMachineParameter
    {

    }

    public abstract class StateMachineParameter<T> : IStateMachineParameter
    {
        public event Action<StateMachineParameter<T>> OnChanged;

        private T _value;
        public virtual T Value {
            get { return _value; }
            set
            {
                //if ((!(_value is ValueType ) && _value == null) || !_value.Equals(value))
                {
                    _value = value;
                    if (OnChanged != null) OnChanged(this);
                }
            }
        }

        public abstract bool Compare(T value, CompareType compareType);

        public abstract void Reset();
    }
}
