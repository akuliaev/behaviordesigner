﻿using Assets.ToonGames.BehaviorDesigner.Runtime.FSM.Conditions;

namespace Assets.ToonGames.BehaviorDesigner.Runtime.FSM.Parameters
{
    public sealed class SmpInt : StateMachineParameter<int>
    {
        public override bool Compare(int value, CompareType compareType)
        {
            switch (compareType)
            {
                case CompareType.Equals: return Value == value;
                case CompareType.NotEqual: return Value != value;
                case CompareType.Greater: return Value > value;
                case CompareType.Less: return Value < value;
                default: return false;
            }
        }

        public override void Reset()
        {
            Value = 0;
        }
    }
}
