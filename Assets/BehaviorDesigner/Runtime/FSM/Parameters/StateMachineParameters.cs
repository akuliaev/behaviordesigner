﻿using System.Collections.Generic;

namespace Assets.ToonGames.BehaviorDesigner.Runtime.FSM.Parameters
{
    public class StateMachineParameters
    {
        private readonly Dictionary<string,IStateMachineParameter> _parameters;

        public StateMachineParameters()
        {
            _parameters = new Dictionary<string, IStateMachineParameter>();
        }

        private void AddParameter<T>(string name, StateMachineParameter<T> parameter)
        {

            if (!_parameters.ContainsKey(name))
                _parameters.Add(name, parameter);
            else
                _parameters[name] = parameter;
        }

        public void AddInt(string name)
        {
            AddParameter(name, new SmpInt());
        }

        public void AddBool(string name)
        {
            AddParameter(name, new SmpBool());
        }

        public void AddFloat(string name)
        {
            AddParameter(name, new SmpFloat());
        }

        public void AddString(string name)
        {
            AddParameter(name, new SmpString());
        }

        public void SetInt(string name, int value)
        {
            SetValue(name, value);
        }

        public void SetBool(string name, bool value)
        {
            SetValue(name, value);
        }

        public void SetFloat(string name, float value)
        {
            SetValue(name, value);
        }

        public void SetString(string name, string value)
        {
            SetValue(name, value);
        }

        public bool Has(string name)
        {
            return _parameters.ContainsKey(name);
        }

        public bool Remove(string name)
        {
            return _parameters.Remove(name);
        }

        private void SetValue<T>(string name, T value)
        {
            if (!Has(name)) return;

            ((StateMachineParameter<T>)_parameters[name]).Value = value;
        }

        public SmpInt GetInt(string name)
        {
            if (!Has(name) || !(_parameters[name] is SmpInt)) return null;

            return (SmpInt)_parameters[name];
        }

        public SmpBool GetBool(string name)
        {
            if (!Has(name) || !(_parameters[name] is SmpBool)) return null;

            return (SmpBool)_parameters[name];
        }

        public SmpFloat GetFloat(string name)
        {
            if (!Has(name) || !(_parameters[name] is SmpFloat)) return null;

            return (SmpFloat)_parameters[name];
        }

        public SmpString GetString(string name)
        {
            if (!Has(name) || !(_parameters[name] is SmpString)) return null;

            return (SmpString)_parameters[name];
        }
    }
}
