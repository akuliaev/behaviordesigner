﻿using Assets.ToonGames.BehaviorDesigner.Runtime.FSM.Conditions;

namespace Assets.ToonGames.BehaviorDesigner.Runtime.FSM.Parameters
{
    public sealed class SmpBool : StateMachineParameter<bool>
    {
        public override bool Compare(bool value, CompareType compareType)
        {
            switch (compareType)
            {
                case CompareType.Equals: return Value == value;
                default: return false;
            }
        }

        public override void Reset()
        {
            Value = false;
        }
    }
}
