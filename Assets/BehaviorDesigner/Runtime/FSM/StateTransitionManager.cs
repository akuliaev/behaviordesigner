﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Assets.ToonGames.BehaviorDesigner.Runtime.FSM
{
    public class StateTransitionManager:IEnumerable<StateTransition>, IEnumerator<StateTransition>
    {

        private Action<StateTransition> _onTransitionOpen;
        public event Action<StateTransition> OnTransitionOpen {
            add { _onTransitionOpen += value; }
            remove { _onTransitionOpen -= value; }
        }

        public bool IsActivated { get; set; }
        public bool AutoCheckConditions { get; set; }

        private readonly List<StateTransition> _transitions;

        private int _index = -1;

        private readonly string _stateName;
        private readonly Action<StateTransition> _callTransitionOpen;

        public StateTransitionManager(string stateName, Action<StateTransition> callTransitionOpen)
        {
            _stateName = stateName;
            _callTransitionOpen = callTransitionOpen;
            _transitions = new List<StateTransition>();    
        }

        public void Activate()
        {
            if (IsActivated) return;
            IsActivated = true;

            foreach (var transition in _transitions)
                transition.Activate();
        }

        public void Deactivate()
        {
            if (!IsActivated) return;
            IsActivated = false;

            foreach (var transition in _transitions)
                transition.Deactivate();
        }

        public StateTransition AddToState(string name)
        {
            var transition = GetToState(name);
            if (transition != null) return transition;

            transition = new StateTransition(_stateName, name);

            transition.OnChangedStatus += OnChangedStatusHandler;
            _transitions.Add(transition);

            if(IsActivated)
                transition.Activate();

            return transition;
        }

        public void RemoveFromState(string name)
        {
            var transition = GetToState(name);

            if (transition == null) return;
            
            transition.OnChangedStatus -= OnChangedStatusHandler;
            _transitions.Remove(transition);

        }

        public StateTransition GetToState(string name)
        {
            return _transitions.Find(c => c.To == name);           
        }

        public bool HasToState(string name)
        {
            return GetToState(name) != null;
        }

        private void OnChangedStatusHandler(StateTransition transition)
        {
            if (transition.Status == StateTransitionStatus.Open)
                DispatchOnTransitionOpenEvent(transition);

        }

        private void DispatchOnTransitionOpenEvent(StateTransition transition)
        {
            if (_onTransitionOpen != null) _onTransitionOpen(transition);
            if (_callTransitionOpen != null) _callTransitionOpen(transition);
        }

        public IEnumerator<StateTransition> GetEnumerator()
        {
            return this;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Dispose()
        {
            
        }

        public bool MoveNext()
        {
            if (_index == _transitions.Count - 1)
            {
                Reset();
                return false;
            }

            _index++;
            return true;
        }

        public void Reset()
        {
            _index = -1;
        }

        public StateTransition Current {
            get { return _transitions[_index]; }
        }

        object IEnumerator.Current
        {
            get { return Current; }
        }
    }
}
