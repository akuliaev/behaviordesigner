﻿namespace Assets.ToonGames.BehaviorDesigner.Runtime.FSM
{
    public interface IStatesFactory
    {
        State CreateState(string state);
    }
}
