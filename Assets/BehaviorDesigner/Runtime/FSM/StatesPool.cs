﻿using System.Collections.Generic;

namespace Assets.ToonGames.BehaviorDesigner.Runtime.FSM
{
    public class StatesPool
    {
        private readonly Dictionary<string, State> _states;

        public StatesPool()
        {
            _states = new Dictionary<string, State>();
        }

        public State GetState(string state)
        {
            if (Has(state))
                return _states[state];

            return null;
        }

        public void AddState(State state)
        {
            if (state == null || Has(state)) return;

            _states.Add(state.Name, state);
        }

        public void RemoveState(State state)
        {
            if (state == null || !Has(state)) return;

            _states.Remove(state.Name);
        }

        public bool Has(string state)
        {
            return _states.ContainsKey(state);
        }

        public bool Has(State state)
        {
            return state != null && _states.ContainsKey(state.Name);
        }
    }
}
