﻿using System;
using Assets.ToonGames.BehaviorDesigner.Runtime.FSM;

namespace Assets.ToonGames.BehaviorDesigner.Runtime
{
    public interface IHasState
    {
        event Action<State> OnChangedState;
        State State { get; }
    }

    public interface IHasBehavior
    {
        StateMachine Behavior { get; }
    }
}
