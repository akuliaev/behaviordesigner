﻿namespace Assets.ToonGames.BehaviorDesigner.Runtime
{
    public abstract class BehaviorStrategy
    {
        public bool IsAcivated { get; private set; }

        protected BehaviorState State { get; private set; }

        public void Activate()
        {
            if (IsAcivated) return;
            IsAcivated = true;
        }

        public void Deactivate()
        {
            if (!IsAcivated) return;
            IsAcivated = false;
        }

        public void Update()
        {
            if (!IsAcivated) return;

            Logic();
        }

        protected abstract void Logic();

        public void SetState(BehaviorState state)
        {
            State = state;
        }
    }
}
