﻿using Assets.ToonGames.BehaviorDesigner.Runtime.FSM;

namespace Assets.ToonGames.BehaviorDesigner.Runtime
{   
    public abstract class BehaviorState : State
    {

        private BehaviorStrategy _behaviorStrategy;

        public BehaviorState(string nameState)
            : base(nameState)
        {

        }

        public void SetStrategy(BehaviorStrategy behaviorStrategy)
        {
            _behaviorStrategy = behaviorStrategy;
            _behaviorStrategy.SetState(this);
        }

        public void Update()
        {
            if (_behaviorStrategy != null)
                _behaviorStrategy.Update();
        }

        public override void Activate()
        {
            base.Activate();

            if (_behaviorStrategy != null)
                _behaviorStrategy.Activate();
        }

        public override void Deactivate()
        {
            base.Deactivate();

            if (_behaviorStrategy != null)
                _behaviorStrategy.Deactivate();
        }
    }
}
